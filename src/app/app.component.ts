import { Component, OnInit } from '@angular/core';
import {LoginComponent} from './login/login.component';
import {RegisterbusinessComponent} from './registerbusiness/registerbusiness.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HttpService } from './http.service';
import { User } from './user';
import { Router } from "@angular/router";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [HttpService]
})
export class AppComponent {
user: User = {id: 99, email: '', password: ''};
show_registration = false;
logout_message = {message: ''};
  constructor(private _httpService: HttpService, private router: Router) { }

  ngOnInit() {
   this._httpService.getcurrentUser()
   .then(user => {
   if (user.email) { this.user = user;
   console.log(this.user);
    }
    });

   }

   userOut(user){
   this.user = user;

   }

  showregistrationOut(show_registration){
   this.show_registration = show_registration;
   }

    onSubmit() {
    this._httpService.logoutUser()
    .then(res => {
       if (res) {
       this.user = {id: 99, email: '', password: ''};
       this.logout_message = res;
       console.log(this.logout_message.message);
      this.router.navigate(['/']);
        }
    });
  }


}
