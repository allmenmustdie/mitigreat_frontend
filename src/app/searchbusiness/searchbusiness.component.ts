import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { Service } from '../service';


@Component({
  selector: 'app-searchbusiness',
  templateUrl: './searchbusiness.component.html',
  styleUrls: ['./searchbusiness.component.css'],
  providers: [HttpService]
})
export class SearchbusinessComponent implements OnInit {
 private LOGO = require("./imgs/menu_icon2.png");
services: Service[] = [];
searches: Service[] = [];
constructor (private _httpService: HttpService) {

}

ngOnInit () {
this._httpService.getAllRegisteredBusiness()
.then(service => {
   this.services = service;
    });


}



onSubmit(search_value) {
this.searches = [];
  for (let service of this.services) {
      if(JSON.parse(JSON.stringify(service.name).toLowerCase()) == search_value.toLowerCase())
      {
      this.searches.push(service)
      }
  }
}


}
