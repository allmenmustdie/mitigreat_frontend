import { Component, OnInit, Output } from '@angular/core';
import { HttpService } from '../http.service';
import { User } from '../user';

@Component({
  selector: 'app-registeruser',
  templateUrl: './registeruser.component.html',
  styleUrls: ['./registeruser.component.css'],
  providers: [HttpService]
})
export class RegisteruserComponent implements OnInit {
  user: User = {id: null, email: '', password: ''};
  errors = [];
  constructor(private _httpService: HttpService) { }

  ngOnInit() {
  }

onSubmit(email, password, password_confirmation ) {
  this._httpService.registerUser(email,password, password_confirmation)
   .then(user => {
   if (user.email) {
   this.user = user;

}
else
{
  this.errors = user;
  console.log(this.errors);
}



   });


}



}
