import {ModuleWithProviders} from '@angular/core';
import {RouterModule, Routes } from '@angular/router'
import {AppComponent} from './app.component';
import {CreatemenuComponent} from './createmenu/createmenu.component';
import {SearchbusinessComponent} from './searchbusiness/searchbusiness.component';
import {RegisterbusinessComponent} from './registerbusiness/registerbusiness.component';
import {LoginComponent} from './login/login.component';
import {RegisteruserComponent} from './registeruser/registeruser.component';
import {ViewmenuComponent} from './viewmenu/viewmenu.component';




export const router: Routes = [
{path: 'login', component: LoginComponent},
{path: 'createmenu/:user_id/:service_id', component: CreatemenuComponent},
{path: 'viewmenu/:service_id', component: ViewmenuComponent},
{path: 'search', component: SearchbusinessComponent},
{path: 'registerbusiness', component: RegisterbusinessComponent}
];


export const routes: ModuleWithProviders = RouterModule.forRoot(router);
