import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { User } from '../user';
import { Service } from '../service';

@Component({
  selector: 'app-registerbusiness',
  templateUrl: './registerbusiness.component.html',
  styleUrls: ['./registerbusiness.component.css'],
   providers: [HttpService]
})
export class RegisterbusinessComponent implements OnInit {
hideme: any = {};
hide_update_form: any = {};
 private LOGO = require("./imgs/menu_icon2.png");
error_message: String = '';
user: User = {id: 99, email: '', password: ''};
service: Service = {id: 99,user_id: null, name: '', type_business: ''};
services: Service[] = [];
show_registration_form = true;
  constructor(private _httpService: HttpService) { }

  ngOnInit() {
     this._httpService.getcurrentUser()
   .then(user => {
   if (user.email) { this.user = user;
    }
    });
  }

onSubmit(name, type_business, shop_number, street_name, city, zip_code, opening_hour, closing_hour, holiday ) {
  this._httpService.registerBusiness(this.user.id, name, type_business, shop_number, street_name, city, zip_code, opening_hour, closing_hour, holiday)
   .then(service => {
   this.service = service;
});


}

toggleshowRegistrationForm() {
  this.show_registration_form = true;
}

getAllServices() {
  this.show_registration_form = false;
    this._httpService.getServices(this.user.id)
   .then(service => {
   if(service.length!=0) {
   this.services = service; }
   else {
  this.error_message = 'No Business is currently registered';
   }
   for (let service of this.services) {
   this.hideme[service.id] = false;
   this.hide_update_form[service.id] = true;
   }

});

}
unregisterBusiness(service) {
this._httpService.unregisterBusiness(service.id);
  this.hideme[service.id] = true;

}

updateService(name, type) {
console.log("hello");
console.log(name);
console.log(type);


}

showupdateform(id) {
  this.hide_update_form[id] = !this.hide_update_form[id];
}


}
