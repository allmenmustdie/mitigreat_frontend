import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { HttpService } from '../http.service';
import { User } from '../user';
import { Router } from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [HttpService]
})
export class LoginComponent implements OnInit {

  user: User = {id: 99, email: '', password: ''};
  @Output() userOut = new EventEmitter();
  @Output() showregistrationOut = new EventEmitter();
  show_registration = false;
  errors = [];
  login_error = {message: ''};
  constructor(private _httpService: HttpService, private router: Router) { }

  ngOnInit() {
  }

onSubmit(email, password ) {
  this._httpService.loginUser(email,password)
   .then(user => {
   if (user.email) {

   this.user = user;
   console.log(this.user);
   this.userOut.emit(this.user);
   this.router.navigate(['']);

}
else
{
  this.login_error = user;
  console.log(this.login_error.message);
}



   });


}

toggleshowRegistration() {
  this.show_registration = true;
  this.showregistrationOut.emit(this.show_registration);
}

}
