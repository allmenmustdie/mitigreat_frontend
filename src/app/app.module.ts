import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {routes} from './app.router';
import { AppComponent } from './app.component';
import { CreatemenuComponent } from './createmenu/createmenu.component';

import { SearchbusinessComponent } from './searchbusiness/searchbusiness.component';
import { RegisterbusinessComponent } from './registerbusiness/registerbusiness.component';
import { LoginComponent } from './login/login.component';
import { NavbarComponent } from './navbar/navbar.component';
import { RegisteruserComponent } from './registeruser/registeruser.component';
import { ViewmenuComponent } from './viewmenu/viewmenu.component';

@NgModule({
  declarations: [
    AppComponent,
    CreatemenuComponent,
    SearchbusinessComponent,
    RegisterbusinessComponent,
    LoginComponent,
    NavbarComponent,
    RegisteruserComponent,
    ViewmenuComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routes
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
