import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpService } from '../http.service';
import { Service } from '../service';
import { Category } from '../category';
import {DomSanitizer} from '@angular/platform-browser';
import { Menuitem } from '../menuitem';

@Component({
  selector: 'app-viewmenu',
  templateUrl: './viewmenu.component.html',
  styleUrls: ['./viewmenu.component.css'],
  providers: [HttpService]
})
export class ViewmenuComponent implements OnInit {
hide_item: any = {};
myitemsarray = [];
mycategories: Category[] = [];
private sub: any;
service_id: number;

  constructor(private _httpService: HttpService, private route: ActivatedRoute, private _sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.myitemsarray = [];
    this.sub = this.route.params.subscribe(params => {
    this.service_id = +params['service_id'];
      this._httpService.getcurrentCategory(this.service_id)
         .then(category => {
         for (let entry of category) {
          this.mycategories.push(entry);
          }

         for (let entry of this.mycategories) {
            entry.image = this._sanitizer.bypassSecurityTrustUrl('data:image/jpg;base64,'+entry.image);
          }
          for (let entry of this.mycategories) {
              this._httpService.getcurrentItems(entry.id)
             .then(item => {
                 for (let singleitem of item) {
                 this.hide_item[singleitem.id] = true;
                    singleitem.image = this._sanitizer.bypassSecurityTrustUrl('data:image/jpg;base64,'+singleitem.image);
                   singleitem.category_id = entry.id
                   this.myitemsarray.push(singleitem);
                 }
              });
          }

      });


   });

 console.log(this.myitemsarray);

  }

  togglehiddenitem(id) {

this.hide_item[id] = !this.hide_item[id];
  }


}
