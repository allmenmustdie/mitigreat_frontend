import { Component, OnInit , OnDestroy, ViewChild} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpService } from '../http.service';
import { ViewmenuComponent } from '../viewmenu/viewmenu.component';
import {DomSanitizer} from '@angular/platform-browser';
import { Router } from '@angular/router';

import { Service } from '../service';
import { Category } from '../category';
import { Menuitem } from '../menuitem';

@Component({

  selector: 'app-createmenu',
  templateUrl: './createmenu.component.html',
  styleUrls: ['./createmenu.component.css'],
  providers: [HttpService]
})
export class CreatemenuComponent implements OnInit, OnDestroy {
@ViewChild(ViewmenuComponent) child:ViewmenuComponent;
myitemsarray = [];
hide: boolean = true;
hide_item: any = {};
hide_item_added: any = {};
services: Service[] = [];
img: any;
 categories: any = [];
 mycategories: Category[] = [];
  user_id: number;
  service_id: number;
  private sub: any;
  category_counter: number = 0;
  constructor(private _httpService: HttpService, private route: ActivatedRoute, private _sanitizer: DomSanitizer, private router: Router ) { }

  ngOnInit() {
      this.sub = this.route.params.subscribe(params => {
       this.user_id = +params['user_id'];
       this.service_id = +params['service_id'];
         this._httpService.getcurrentBusiness(this.service_id)
         .then(service => {
          this.services.push(service);
          });
          this._httpService.getcurrentCategory(this.service_id)
                 .then(category => {
                 for (let entry of category) {
              this.mycategories.push(entry);
              }

               for (let entry of this.mycategories) {
                  entry.image = this._sanitizer.bypassSecurityTrustUrl('data:image/jpg;base64,'+entry.image);
                  this.hide_item[entry.id] = true;
                  this._httpService.getcurrentItems(entry.id)
                  .then(item => {
                     for (let singleitem of item) {
                        singleitem.image = this._sanitizer.bypassSecurityTrustUrl('data:image/jpg;base64,'+singleitem.image);
                       singleitem.category_id = entry.id
                       this.myitemsarray.push(singleitem);
                     }
                  });
               }
          });
      });

  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }

addCategory() {
  this.hide=!this.hide;

}
fileChange(input){
  this.readFiles(input.files);
}
readFile(file, reader, callback){
  reader.onload = () => {
    callback(reader.result);
  }

  reader.readAsDataURL(file);
}
readFiles(files, index=0){
  // Create the file reader
  let reader = new FileReader();

  // If there is a file
  if(index in files){
    this.readFile(files[index], reader, (result) =>{
      this.img = result;


    });
  }
}

registerCategory(name) {
      this.hide = true;
       this._httpService.registerCategory(this.service_id, name, this.img).then(category => {
       category.image = this._sanitizer.bypassSecurityTrustUrl('data:image/jpg;base64,'+category.image);
       this.hide_item[category.id] = true;
        this.mycategories.push(category);
        });
        console.log(this.mycategories);
}

removeCategory(category_id, category) {
  var index = this.mycategories.indexOf(category, 0);
  if (index > -1) {
     this.mycategories.splice(index, 1);
  }
         this._httpService.removeCategory(category_id)
         .then(message => {
         console.log(message);
        });


}

showmenuItem(category) {
  this.hide_item[category.id] = !this.hide_item[category.id];
}


registeritem(id,name, description, serving_time, price, served_with) {
       this.hide_item_added[id] = false;
       this._httpService.registeritem(id, name, description, serving_time, price, served_with,this.img).then(menuitem => {
       menuitem.category_id = id;
       menuitem.image = this._sanitizer.bypassSecurityTrustUrl('data:image/jpg;base64,'+menuitem.image);
        this.myitemsarray.push(menuitem);
        this.hide_item[id] = !this.hide_item[id];
        });

}
removeItem(item_id, item) {
  var index = this.myitemsarray.indexOf(item, 0);
  if (index > -1) {
     this.myitemsarray.splice(index, 1);
  }
         this._httpService.removeItem(item_id)
         .then(message => {
         console.log(message);
        });


}
previewcard() {
  this.router.navigate(['/viewmenu',this.service_id]);
}

}
