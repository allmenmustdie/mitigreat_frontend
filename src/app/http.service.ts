import {Injectable} from '@angular/core';
import { User } from './user';
import { URLSearchParams } from "@angular/http"
import {Headers} from "@angular/http"


import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class HttpService {
constructor(public http: Http) { }

getAllUsers(): Promise<User[]> {
  return this.http.get('http://localhost:3000/users.json')
  .toPromise()
  .then(response => response.json() as User);
}

registerUser(email, password, password_confirmation): any {
var headers = new Headers();

headers.append('Content-Type', 'application/json');
 return this.http
    .post('http://localhost:3000/users', JSON.stringify({"user":{email: email, password: password, password_confirmation: password_confirmation}}), {headers:headers})
   .toPromise()
    .then(res => res.json())
}

loginUser(email, password): any {
var headers = new Headers();

headers.append('Content-Type', 'application/json');
 return this.http
    .post('http://localhost:3000/login', JSON.stringify({"session":{email: email, password: password}}), {headers:headers})
   .toPromise()
    .then(res => res.json())
}

logoutUser(): any {
var headers = new Headers();

headers.append('Content-Type', 'application/json');
 return this.http
    .get('http://localhost:3000/logout', {headers:headers})
   .toPromise()
    .then(res => res.json())
}

getcurrentUser(): any {
var headers = new Headers();

headers.append('Content-Type', 'application/json');
 return this.http
    .get('http://localhost:3000/currentuser', {headers:headers})
   .toPromise()
    .then(res => res.json())
}

  registerBusiness(id, name, type_business, shop_number, street_name, city, zip_code, opening_hour, closing_hour, holiday): any {
  var headers = new Headers();

  headers.append('Content-Type', 'application/json');
   return this.http
      .post('http://localhost:3000/users/'+id+'/services', JSON.stringify({"id": id, "service":{name: name, type_business: type_business,shop_number: shop_number, street_name: street_name, city: city, zipcode: zip_code, opening_hour: opening_hour, closing_hour: closing_hour, holiday: holiday }}), {headers:headers})
     .toPromise()
      .then(res => res.json())

  }
  getServices(id): any {
  var headers = new Headers();

  headers.append('Content-Type', 'application/json');
   return this.http
      .get('http://localhost:3000/users/'+id+'/services', {headers:headers})
         .toPromise()
          .then(res => res.json())

  }
  getAllRegisteredBusiness(){
  var headers = new Headers();

  headers.append('Content-Type', 'application/json');
   return this.http
      .get('http://localhost:3000//allservices', {headers:headers})
         .toPromise()
          .then(res => res.json())

  }

  getcurrentBusiness(id) {
  var headers = new Headers();

  headers.append('Content-Type', 'application/json');
  return this.http
    .post('http://localhost:3000/currentservice', JSON.stringify({"id": id}), {headers:headers})
   .toPromise()
    .then(res => res.json())

  }
registerCategory(id, name, image) {
  var headers = new Headers();

  headers.append('Content-Type', 'application/json');


    return this.http
    .post('http://localhost:3000/services/'+id+'/sections',JSON.stringify({"id": id, "section":{name: name, image: image }}) , {headers:headers})
   .toPromise()
    .then(res => res.json())
}

getcurrentCategory(id) {
  var headers = new Headers();
  headers.append('Content-Type', 'application/json');
    return this.http
    .post('http://localhost:3000/getcategorybyservice',JSON.stringify({"id": id}) , {headers:headers})
   .toPromise()
    .then(res => res.json())

}


unregisterBusiness(id) {
console.log(id);
    var headers = new Headers();
  headers.append('Content-Type', 'application/json');
    return this.http
    .delete('http://localhost:3000/services/'+id, {headers:headers})
   .toPromise()
    .then(res => res.json())
}


removeCategory(id) {
var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http
    .delete('http://localhost:3000/deletecategory/'+id, {headers:headers})
   .toPromise()
    .then(res => res.json())

}


registeritem(id, name, description, serving_time, price, served_with,image) {
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http
    .post('http://localhost:3000/sections/'+id+'/menuitems',JSON.stringify({"id": id, "menuitem":{name: name, image: image, description: description, serving_time: serving_time, price: price, served_with: served_with }}) , {headers:headers})
   .toPromise()
    .then(res => res.json())
}

getcurrentItems(id) {
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http
    .post('http://localhost:3000/getitembysection',JSON.stringify({"id": id}) , {headers:headers})
   .toPromise()
    .then(res => res.json())
}

removeItem(id) {
var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http
    .delete('http://localhost:3000/deleteitem/'+id, {headers:headers})
   .toPromise()
    .then(res => res.json())

}

}

